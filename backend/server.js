const express = require("express");
const cors = require("cors");
const rooms = require("./rooms.js");
const bodyParser = require("body-parser");
const app = express();
var path = require('path');

app.use('/', express.static(path.join(__dirname, 'public')))

function getRoom(roomId) {
  let room = null;

  rooms.forEach((p) => {
    if (p.sys.id == roomId) {
      room = p;
    }
  });

  return room;
}

app.use(cors());
app.use(bodyParser.json());

app.listen(4141, function () {
  console.log("Listening on port 4141...");
});

app.get("/rooms", (req, res) => {
  res.json(rooms);
});

app.get("/rooms/:id", (req, res) => {
  let room = getRoom(req.params.id);
  if (room) {
    res.json(room);
  } else {
    res.sendStatus(404);
  }
});
