const formatData = (data) => {
  if (Array.isArray(data)) {
    return data.map(item => {
      let id = item.sys.id;
      let images = item.fields.images.map(image => image.fields.file.url);
      let room = {...item.fields, images, id};
      return room;
    });
  } else {
    let id = data.sys.id;
    let images = data.fields.images.map(image => image.fields.file.url);
    let room = {...data.fields, images, id};
    return room
  }
}

const featuredRoom = (data) => {
  return data.filter(item => {
    return item.featured === true;
  });
}

export {formatData, featuredRoom};
