import Vue from "vue";
import Vuex from "vuex";
import VueResource from "vue-resource";
import { formatData } from "./../helper";
Vue.use(Vuex);

Vue.use(VueResource);

const getUnique = (items, value) => {
  return [...new Set(items.map(item => item[value]))];
};

const store = new Vuex.Store({
  state: {
    rooms: [],
    sortedRooms: [],
    filter: {}
  },
  getters: {
    rooms: state => {
      let newRooms = state.rooms;
      let maxDefaultSize = Math.max(...state.rooms.map(item => item.size));
      if (Object.keys(state.filter).length !== 0) {
        let { type, capacity, price, minSize, maxSize, breakfast, pets } = state.filter;
        minSize = minSize ? minSize : 0;
        maxSize = maxSize ? maxSize : maxDefaultSize;
        if (type && type !== "all") {
          newRooms = newRooms.filter(room => room.type === type);
        }
        if (capacity && capacity !== 1) {
          newRooms = newRooms.filter(room => room.capacity >= capacity);
        }
        if (price) {
          newRooms = newRooms.filter(room => room.price <= price);
        }

        newRooms = newRooms.filter(
          room => room.size <= maxSize && room.size >= minSize
        );

        if (breakfast) {
          newRooms = newRooms.filter(room => room.breakfast === true);
        }

        if (pets) {
          newRooms = newRooms.filter(room => room.pets === true);
        }
      }
      return newRooms;
    },
    types: state => ["all", ...getUnique(state.rooms, "type")],
    capacities: state => getUnique(state.rooms, "capacity"),
    maxPrice: state => Math.max(...state.rooms.map(item => item.price)),
    maxSize: state => Math.max(...state.rooms.map(item => item.size)),
    price: state => state.filter.price
  },
  mutations: {
    FETCH_ROOMS(state, rooms) {
      state.rooms = formatData(rooms);
    },
    UPDATE_FILTER(state, payload) {
      state.filter = payload;
    }
  },
  actions: {
    fetchRooms({ commit }, { self }) {
      Vue.http
        .get("http://localhost:4141/rooms")
        .then(response => {
          const roomList = JSON.parse(JSON.stringify(response.body));
          commit("FETCH_ROOMS", roomList);
          self.filterRooms();
        })
        .catch(error => {
          console.log(error.statusText);
        });
    },
    filterRooms({ commit }, filterObj) {
      commit("UPDATE_FILTER", filterObj);
    }
  }
});

export default store;
