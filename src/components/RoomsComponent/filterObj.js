import store from "./../../store";
console.log(store.getters.maxPrice);
console.log(store.getters);
const filterObj = {
  _type: "all",
  _capacity: 1,
  _price: 0,
  _minSize: 0,
  _maxSize: 0,
  _breakfast: false,
  _pets: false
};

Object.defineProperty(filterObj, "type", {
  enumerable: true,
  get() {
    return this._type;
  },
  set(newVal) {
    if (newVal === "") {
      this._type = undefined;
    } else {
      this._type = newVal;
    }
  }
});

Object.defineProperty(filterObj, "capacity", {
  enumerable: true,
  get() {
    return this._capacity;
  },
  set(newVal) {
    if (newVal === "") {
      this._capacity = undefined;
    } else {
      this._capacity = newVal;
    }
  }
});

Object.defineProperty(filterObj, "price", {
  enumerable: true,
  get() {
    return this._price;
  },
  set(newVal) {
    if (newVal === "") {
      this._price = undefined;
    } else {
      this._price = newVal;
    }
  }
});

Object.defineProperty(filterObj, "minSize", {
  enumerable: true,
  get() {
    return this._minSize;
  },
  set(newVal) {
    if (newVal === "") {
      this._minSize = undefined;
    } else {
      this._minSize = newVal;
    }
  }
});

Object.defineProperty(filterObj, "maxSize", {
  enumerable: true,
  get() {
    return this._maxSize;
  },
  set(newVal) {
    if (newVal === "") {
      this._maxSize = undefined;
    } else {
      this._maxSize = newVal;
    }
  }
});

Object.defineProperty(filterObj, "breakfast", {
  enumerable: true,
  get() {
    return this._breakfast;
  },
  set(newVal) {
    if (newVal === "") {
      this._breakfast = undefined;
    } else {
      this._breakfast = newVal;
    }
  }
});

Object.defineProperty(filterObj, "pets", {
  enumerable: true,
  get() {
    return this._pets;
  },
  set(newVal) {
    if (newVal === "") {
      this._pets = undefined;
    } else {
      this._pets = newVal;
    }
  }
});

export default filterObj;
