import Home from "./../components/HomeComponent/Home";
import Rooms from "../components/RoomsComponent/Rooms.vue";
import ViewRoom from "../components/ViewRoom.vue";

const routes = [
  { path: "", name: "Home", component: Home },
  { path: "/rooms/", name: "Rooms", component: Rooms },
  {
    path: "/rooms/:roomId",
    name: "ViewRoom",
    props: true,
    component: ViewRoom,
  },
  { path: "*", component: { template: "<h1>Page Not Found!</h1>" } },
];

export default routes;
